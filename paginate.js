const paginate = (followers) => {
  const itemPerPage = 9;
  console.log(followers);
  const numberOfPages = Math.ceil(followers.length / itemPerPage);

  const newFollowers = Array.from({ length: numberOfPages }, (_, index) => {
    const start = index * itemPerPage;

    return followers.slice(start, start + itemPerPage);
  });

  console.log(newFollowers);
  return newFollowers;
};

export default paginate;
